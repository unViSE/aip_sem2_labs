from random import randint
from src.lab2.lib.SearchAlgos import (LinearSearch, BinarySearch,
                                      NaiveStringSearch, KMPStringSearch)


# TODO: Rework benchmark
def callable_benchmark_calls(start, end, step):
    """Callable benchmark for finding the number of operations"""
    average_calls = ([], [], [], [])
    size_of_array = []

    for size in range(start, end, step):
        array = [randint(-64, 63) for i in range(size)]
        string = ''.join(["A" for i in range(size)])

        average_calls[0].append(LinearSearch(array, 128).calls('temp',
                                                               lines_to_print='compare'))
        average_calls[1].append(BinarySearch(array, 128).calls('temp',
                                                               lines_to_print='compare'))
        average_calls[2].append(NaiveStringSearch(string, "B").calls('temp',
                                                                     lines_to_print='compare'))
        average_calls[3].append(KMPStringSearch(string, "B").calls('temp',
                                                                   lines_to_print='compare'))

        size_of_array.append(size)

        print(f"Size of array is {size}")
        print(f"Average calls (linear search) = {average_calls[0][-1]}, (binary search) = {average_calls[1][-1]},",
              f"(naive string search) = {average_calls[2][-1]}, (KMP search) = {average_calls[3][-1]}")

    return size_of_array, average_calls
