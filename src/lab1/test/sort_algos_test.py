import pytest

from src.lab1.lib import sort_algo as sa

array = [(12, 3, 3), (4, 5, 6), (6, 5, 3), (83, 3, 3), (6, 8, 3), (83, 5, 2), (34, 6, 2), (83, 7, 9)]

key = lambda x: x[0]

# CONST for test
SORTED_ARRAY = sorted(array)
SORTED_REVERSE_ARRAY = sorted(array, reverse=True)


def test_correct_data():
    try:
        sorted(array, key=key)
    except IndexError:
        assert False
    else:
        assert True


# params for parametrized test
ascending_sort_param = [(sa.bubble_sort(array, key), SORTED_ARRAY),
                        (sa.insertion_sort(array, key), SORTED_ARRAY),
                        (sa.shell_sort(array, key), SORTED_ARRAY),
                        (sa.quick_sort(array, key), SORTED_ARRAY)]

descending_sort_param = [(sa.bubble_sort(array, key, reverse=True), SORTED_REVERSE_ARRAY),
                         (sa.insertion_sort(array, key, reverse=True), SORTED_REVERSE_ARRAY),
                         (sa.shell_sort(array, key, reverse=True), SORTED_REVERSE_ARRAY),
                         (sa.quick_sort(array, key, reverse=True), SORTED_REVERSE_ARRAY)]


def base_comparison(test_input, excepted, index=0):
    """Base comparison for all sort test

    @param test_input: class
        What was received
    @param excepted: class
        What was excepted
    @param index: int, optional. Default is 0
        Item index
    @return: Void"""

    for i in range(len(test_input)):
        if test_input[i][index] != excepted[i][index]:
            assert False
    assert True


@pytest.mark.parametrize("test_input, excepted", ascending_sort_param)
def test_ascending_sort(test_input, excepted):
    base_comparison(test_input, excepted)


@pytest.mark.parametrize("test_input, excepted", descending_sort_param)
def test_descending_sort(test_input, excepted):
    base_comparison(test_input, excepted)


ascending_stable_sort_param = [(sa.bubble_sort(array, key), SORTED_ARRAY),
                               (sa.insertion_sort(array, key), SORTED_ARRAY)]

descending_stable_sort_param = [(sa.bubble_sort(array, key, reverse=True), SORTED_REVERSE_ARRAY),
                                (sa.insertion_sort(array, key, reverse=True), SORTED_REVERSE_ARRAY)]


@pytest.mark.parametrize("test_input, excepted", ascending_stable_sort_param)
def test_stable_ascending_sort(test_input, excepted):
    base_comparison(test_input, excepted, index=1)


@pytest.mark.parametrize("test_input, excepted", descending_stable_sort_param)
def test_stable_descending_sort(test_input, excepted):
    base_comparison(test_input, excepted, index=1)