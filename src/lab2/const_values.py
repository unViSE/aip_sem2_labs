# CPU: INTEL(R) CORE(TM) i5-6600 @ 3.30 GHz - 6 Mb
TEST_BEGIN = 10_000
TEST_END = 100_000
TEST_STEP = 10_000


REAL_BEGIN = 1_000_000
REAL_END = 10_000_000
REAL_STEP = 1_000_000


SEARCH_ALGORITHMS = ['LinearSearch', 'BinarySearch', 'NaiveStringSearch', 'KMPSearch']
