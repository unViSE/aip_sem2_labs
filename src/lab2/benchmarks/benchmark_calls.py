import matplotlib.pyplot as plt
from src.lab2.benchmarks.ready_made_benchmarks_as_functions import callable_benchmark_calls
from numpy import polyfit, poly1d, array, log2
from src.utils.theoretical_complexity import (linear_complexity, logarithmic_complexity,
                                              quadratic_complexity)
from src.lab2.const_values import (TEST_BEGIN, TEST_END, TEST_STEP,
                                   REAL_BEGIN, REAL_END, REAL_STEP,
                                   SEARCH_ALGORITHMS)


test_size_of_array, test_average_calls = callable_benchmark_calls(TEST_BEGIN, TEST_END, TEST_STEP)
real_size_of_array = list(range(REAL_BEGIN, REAL_END, REAL_STEP))

p0_coff = polyfit(test_size_of_array, test_average_calls[0], 1)
p1_coff = polyfit(test_size_of_array, test_average_calls[1], 1)
p2_coff = polyfit(test_size_of_array, test_average_calls[2], 2)
p3_coff = polyfit(test_size_of_array, test_average_calls[3], 1)

p0 = poly1d(p0_coff)
p1 = poly1d(p1_coff)
p2 = poly1d(p2_coff)
p3 = poly1d(p3_coff)

# Polyfit can only tweak by int degree. Using curve fit from pandas is too hard.
# Therefore, the initial array is completed linearly, and then rebuilt according to its complexity.
binary_search = array(log2(p1(real_size_of_array)))

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Matplotlib for visualization Big O of algorithms
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
fig, ax = plt.subplots(2, 2, figsize=(15, 10))


# Practical graphs
ax[0, 0].plot(real_size_of_array, p0(real_size_of_array), '-o',
              color='orange', label=f"{SEARCH_ALGORITHMS[0]}Worst(Practical)")

# Theoretical graphs
ax[0, 0].plot(real_size_of_array, [i for i in linear_complexity(real_size_of_array)],
              color='darkblue', label=f"{SEARCH_ALGORITHMS[0]}Worst(Theoretical)")


# Practical graphs
ax[0, 1].plot(real_size_of_array, binary_search, '-o',
              color='orange', label=f"{SEARCH_ALGORITHMS[1]}Worst(Practical)")

# Theoretical graphs
ax[0, 1].plot(real_size_of_array, [i for i in logarithmic_complexity(real_size_of_array)],
              color='darkblue', label=f"{SEARCH_ALGORITHMS[1]}Worst(Theoretical)")


# Practical graphs
ax[1, 0].plot(real_size_of_array, p2(real_size_of_array), '-o',
              color='orange', label=f"{SEARCH_ALGORITHMS[2]}Worst(Practical)")

# Theoretical graphs
ax[1, 0].plot(real_size_of_array, [i for i in quadratic_complexity(real_size_of_array)],
              'darkblue', label=f"{SEARCH_ALGORITHMS[2]}Worst(Theoretical)")


# Practical graphs
ax[1, 1].plot(real_size_of_array, p3(real_size_of_array), '-o',
              color='orange', label=f"{SEARCH_ALGORITHMS[3]}Worst(Practical)")

# Theoretical graphs
ax[1, 1].plot(real_size_of_array, [i for i in linear_complexity(real_size_of_array)],
              color='darkblue', label=f"{SEARCH_ALGORITHMS[3]}Worst(Theoretical)")

index = 0
for a_x in ax.flat:
    a_x.set(title=SEARCH_ALGORITHMS[index],
            xlabel='Array size', ylabel='Number of operations')
    a_x.grid(), a_x.legend()
    index += 1

plt.show()

