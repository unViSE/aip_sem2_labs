import matplotlib.pyplot as plt

from src.lab1.benchmarks.ready_made_benchmarks_as_functions import callable_benchmark_time
from numpy import polyfit, poly1d

from src.lab1.benchmarks.const_for_benchmarks import (TEST_BEGIN, TEST_END, TEST_STEP,
                                                      REAL_BEGIN, REAL_END, REAL_STEP,
                                                      SORTED_ALGORITHMS)


test_size_of_array, test_time = callable_benchmark_time(TEST_BEGIN, TEST_END, TEST_STEP)
real_size_of_array = list(range(REAL_BEGIN, REAL_END, REAL_STEP))

p0_coff = polyfit(test_size_of_array, test_time[0], 2)
p1_coff = polyfit(test_size_of_array, test_time[1], 2)
p2_coff = polyfit(test_size_of_array, test_time[1], 1)
p3_coff = polyfit(test_size_of_array, test_time[2], 1)

p0 = poly1d(p0_coff)
p1 = poly1d(p1_coff)
p2 = poly1d(p2_coff)
p3 = poly1d(p3_coff)

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Matplotlib for visualization time of algorithms
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
fig, ax = plt.subplots(2, 2, figsize=(15, 10))

index = 0
for ax_ in ax.flat:
    ax_.set(title=SORTED_ALGORITHMS[index], xlabel='Array size', ylabel='Time, s',
            label=SORTED_ALGORITHMS[index])
    ax_.grid(), ax_.legend()
    index += 1


ax[0, 0].plot(real_size_of_array, p0(real_size_of_array), 'blue')
ax[0, 1].plot(real_size_of_array, p1(real_size_of_array), 'blue')
ax[1, 0].plot(real_size_of_array, p2(real_size_of_array), 'blue')
ax[1, 1].plot(real_size_of_array, p3(real_size_of_array), 'blue')


plt.show()
