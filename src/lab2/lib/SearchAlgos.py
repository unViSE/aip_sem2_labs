from src.lab2.lib.helper_functions import get_prefix_as_array, compare
from src.utils.useful_functions import remove_files_from_dir
import cProfile
import pstats


class Search:
    """Class <Search> for to implement inheritance

    @param seq: list, tuple
        The original sequence in which you will need to find target
    @param target: var
        The variable to be found in the original sequence
    """
    def __init__(self, seq, target):
        self.seq = seq
        self.target = target

    def search(self):
        """Abstract method for finding a target in a sequence

        @return: NotImplementedError
        """
        raise NotImplementedError

    def calls(self, file_name, lines_to_print):
        file_name = f'{file_name}.txt'

        with open(file_name, 'w') as f:
            pr = cProfile.Profile()
            pr.enable()
            self.search()
            pr.disable()
            p = pstats.Stats(pr, stream=f)
            p.strip_dirs().sort_stats().print_stats(lines_to_print)

        with open(file_name) as f:
            lines = f.readlines()
            lines = [line.strip() for line in lines]
            lines = [line for line in lines if line not in '']
        remove_files_from_dir('txt')
        return int(lines[4].split()[0])


class LinearSearch(Search):
    """Class for implementing linear search for DataStructures"""
    def search(self):
        """
        @return target: var
        """
        for index, element in enumerate(self.seq):
            if compare(element, self.target):
                return index
        return None


class BinarySearch(Search):
    """Class for implementing binary search for DataStructures"""
    def search(self, start=0, end=None):
        """
        @return target: var
        """
        if end is None:
            end = len(self.seq)

        if start > end:
            return None

        mid = (start + end) // 2

        if compare(self.target, self.seq[mid], choice=2):
            return self.search(start, mid-1)
        elif compare(self.target, self.seq[mid], choice=1):
            return self.search(mid+1, end-1)
        else:
            return mid


class NaiveStringSearch(Search):
    """Class for implementing naive search for Strings"""
    def search(self):
        """
        @return target: var
        """
        for i in range(len(self.seq)):
            for j in range(i, len(self.seq)):
                if compare(self.seq[i:j], self.target):
                    return i
        return None


class KMPStringSearch(Search):
    """Class for implementing naive search for Strings"""
    def search(self):
        """
        @return target: var
        """
        seq_size, target_size = len(self.seq), len(self.target)
        prefix_array = get_prefix_as_array(self.target, target_size)

        m, n = 0, 0

        while m != seq_size:
            if compare(self.seq[m], self.target[n]):
                m += 1
                n += 1
            else:
                n = prefix_array[n - 1]

            if compare(n, target_size):
                return m - n
            elif compare(n, 0):
                m += 1
        return None
