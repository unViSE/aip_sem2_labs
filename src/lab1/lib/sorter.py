def replace(array, first, second, choice=0):
    """Function to replace one element for another"""
    if choice == 0:
        array[first] = array[second]
    elif choice == 1:
        array[first] = second


def swap(array, first, second):
    """Function to swap the elements of one array"""
    array[first], array[second] = array[second], array[first]


def partition(array, begin, end, key):
    """Function for selecting a pivot"""
    pivot = key(array[(begin + end) // 2])
    i = begin
    j = end

    while i <= j:
        while key(array[i]) < pivot:
            i += 1

        while key(array[j]) > pivot:
            j -= 1

        if i <= j:
            swap(array, i, j)

            i, j = i+1, j-1

    return i
