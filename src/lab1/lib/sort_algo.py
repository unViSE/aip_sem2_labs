from copy import deepcopy
from src.lab1.lib.sorter import swap, replace, partition


def bubble_sort(data, key=lambda x: x, reverse=False):
    """Sort the array in ascending order using bubble sorting method

    @param data: list.
        A list filled with a random numbers
    @param key: class <function>, optional. Default is lambda x: x
        Key for sorting the array, need for 2D or more D array
    @param reverse: bool, optional
        Needed to return an inverted sorted array
    @return: The sorted list in ascending or descending order"""

    if not data:
        return data

    seq = deepcopy(data)

    changed = True
    while changed:
        changed = False
        for i in range(len(seq) - 1):
            if key(seq[i]) > key(seq[i + 1]):
                swap(seq, i, i + 1)
                changed = True

    if not reverse:
        return seq
    else:
        return seq[::-1]


def insertion_sort(data, key=lambda x: x, reverse=False):
    """Sort the array in ascending order using insertion sorting method

    @param data: list.
        A list filled with a random numbers
    @param key: class <function>, optional. Default is lambda x: x
        Key for sorting the array, need for 2D or more D array
    @param reverse: bool, optional
        Needed to return an inverted sorted array
    @return: The sorted list in ascending or descending order"""
    if not data:
        return data

    seq = deepcopy(data)

    for index in range(1, len(seq)):
        current_value = seq[index]
        current_position = index - 1

        while current_position >= 0 and key(current_value) < key(seq[current_position]):
            replace(seq, current_position+1, current_position)
            current_position = current_position - 1

        replace(seq, current_position+1, current_value, choice=1)

    if not reverse:
        return seq
    else:
        return seq[::-1]


def shell_sort(data, key=lambda x: x, reverse=False):
    """Sort the array in ascending order using Shell sorting method

    @param data: list.
        A list filled with a random numbers
    @param key: class <function>, optional. Default is lambda x: x
        Key for sorting the array, need for 2D or more D array
    @param reverse: bool, optional
        Needed to return an inverted sorted array
    @return: The sorted list in ascending or descending order"""

    if not data:
        return data

    seq = deepcopy(data)

    length = len(seq)
    step = length//2

    while step >= 1:
        for i in range(step, length):
            current = seq[i]
            j = i
            while j > 0 and key(seq[j - step]) > key(current):
                replace(seq, j, j-step)
                j -= step
            replace(seq, j, current, choice=1)
        step = step//2

    if not reverse:
        return seq
    else:
        return seq[::-1]


def quick_sort(data, key=lambda x: x, reverse=None):
    """Sort the array in ascending order using Shell sorting method

    @param data: list.
        A list filled with a random numbers
    @param key: class <function>, optional. Default is lambda x: x
        Key for sorting the array, need for 2D or more D array
    @param reverse: bool, optional
        Needed to return an inverted sorted array
    @return: The sorted list in ascending or descending order"""

    if not data:
        return data

    seq = deepcopy(data)

    def _quicksort(array, begin, end, key):
        """Additional function for convenient calling"""
        if begin < end:
            split = partition(array, begin, end, key)
            _quicksort(array, begin, split-1, key)
            _quicksort(array, split, end, key)

    # begin - the starting index from which to sort the array
    # end - the final index from which to sort the array
    # ~they need for the partition function and recurse
    _quicksort(seq, 0, len(data)-1, key)

    if not reverse:
        return seq
    else:
        return seq[::-1]
