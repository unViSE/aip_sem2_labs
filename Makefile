.PHONY: all test

SRC_DIR := src
UTILS_DIR := utils
TEST_DIR := test
VENV_DIR := *env


all:install git-setup install

git-setup: $(UTILS_DIR)/git-script.sh
	git-script.sh

test:
	pytest -x $(TEST_DIR)

install: uninstall
	sudo apt install python3
	sudo apt install -y python3-venv
	python3 -m venv venv
	source $(VENV_DIR)/bin/activate
	pip install -r requirements.txt
	pip install -e .

uninstall:
	rm -rf $(VENV_DIR)

clean:
	rm -rf .idea .vscode .pytest_cache __pycache__/ *.egg-info

deploy: test uninstall clean