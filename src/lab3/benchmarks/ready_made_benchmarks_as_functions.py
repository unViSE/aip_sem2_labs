from src.lab3.lib.graph import Graph


def callable_benchmark_calls(start, end, step):
    average_calls = ([])
    size_of_array = []

    graph = Graph()
    for size in range(start, end, step):
        graph.set_complete_graph_from_nx(size)
        average_calls.append(graph.calls('temp', 'compare'))

        size_of_array.append(size)

        print(f"\nSize of array is {size}")
        print(f"Graph calls is {average_calls[-1]}")

    return size_of_array, average_calls
