import matplotlib.pyplot as plt

from src.lab1.benchmarks.ready_made_benchmarks_as_functions import callable_benchmark_calls
from numpy import polyfit, poly1d
from src.utils.useful_functions import remove_files_from_dir

from src.utils.theoretical_complexity import (quadratic_complexity, n_logarithm_n_complexity,
                                              n_logarithmic_power_complexity)

from src.lab1.benchmarks.const_for_benchmarks import (TEST_BEGIN, TEST_END, TEST_STEP,
                                                      REAL_BEGIN, REAL_END, REAL_STEP,
                                                      SORTED_ALGORITHMS)


test_size_of_array, test_average_calls = callable_benchmark_calls(TEST_BEGIN, TEST_END, TEST_STEP)
real_size_of_array = list(range(REAL_BEGIN, REAL_END, REAL_STEP))

p0_coff = polyfit(test_size_of_array, test_average_calls[0], 2)
p1_coff = polyfit(test_size_of_array, test_average_calls[1], 2)
p2_coff = polyfit(test_size_of_array, test_average_calls[1], 1)
p3_coff = polyfit(test_size_of_array, test_average_calls[2], 1)

p0 = poly1d(p0_coff)
p1 = poly1d(p1_coff)
p2 = poly1d(p2_coff)
p3 = poly1d(p3_coff)


# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Matplotlib for visualization Big O of algorithms
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
fig, ax = plt.subplots(2, 2, figsize=(15, 10))


# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Plot for Bubble sort
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# Practical graphs
ax[0, 0].plot(real_size_of_array, p0(real_size_of_array),
              'orange', label="Average(Practical)")

# Theoretical graphs
ax[0, 0].plot(real_size_of_array, quadratic_complexity(real_size_of_array),
              'darkblue', label="Average and Worst(Theoretical)")


# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Plot for Insertion sort
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# Practical graphs
ax[0, 1].plot(real_size_of_array, p1(real_size_of_array),
              'orange', label="Average(Practical)")

# Theoretical graphs
ax[0, 1].plot(real_size_of_array, quadratic_complexity(real_size_of_array),
              'darkblue', label="Average and Worst(Theoretical)")


# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Plot for Shell sort
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# Practical graphs
ax[1, 0].plot(real_size_of_array, p2(real_size_of_array),
              'orange', label="Average(Practical)")

# Theoretical graphs
ax[1, 0].plot(real_size_of_array, n_logarithmic_power_complexity(real_size_of_array, deg=2),
              'darkblue', label="Worst(Theoretical)")


# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Plot for Quick sort
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# Practical graphs
ax[1, 1].plot(real_size_of_array, p3(real_size_of_array),
              'orange', label="Average(Practical)")

# Theoretical graphs
ax[1, 1].plot(real_size_of_array, n_logarithm_n_complexity(real_size_of_array),
              'royalblue', label="Best and Average(Theoretical)")

index = 0
for a_x in ax.flat:
    a_x.set(title=SORTED_ALGORITHMS[index],
            xlabel='Array size', ylabel='Number of operations')
    a_x.grid(), a_x.legend()
    index += 1

plt.show()

remove_files_from_dir(type_of_file="txt")