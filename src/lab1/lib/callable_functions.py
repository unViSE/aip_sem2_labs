from src.utils.benchmark_decorators import (elapsed_time, occupied_memory,
                                            multiple_call_decorator)

from src.lab1.lib.sort_algo import (bubble_sort, insertion_sort,
                                    shell_sort, quick_sort)


@multiple_call_decorator(elapsed_time, occupied_memory)
def bubble_sort_with_decorators(array, key=lambda x: x, reverse=False):
    result = bubble_sort(array, key=key, reverse=reverse)
    return result


@multiple_call_decorator(elapsed_time, occupied_memory)
def insertion_sort_with_decorators(array, key=lambda x: x, reverse=False):
    result = insertion_sort(array, key=key, reverse=reverse)
    return result


@multiple_call_decorator(elapsed_time, occupied_memory)
def shell_sort_with_decorators(array, key=lambda x: x, reverse=False):
    result = shell_sort(array, key=key, reverse=reverse)
    return result


@multiple_call_decorator(elapsed_time, occupied_memory)
def quick_sort_with_decorators(array, key=lambda x: x, reverse=False):
    result = quick_sort(array, key=key, reverse=reverse)
    return result
