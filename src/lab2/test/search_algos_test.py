import pytest
from src.lab2.lib.SearchAlgos import (LinearSearch, BinarySearch,
                                      NaiveStringSearch, KMPStringSearch)


# consts for test
array = [12, 3, 6, 5, 3, 83, 5, 2, 34, 14]
string = "Pycharm Professional Edition"

# params for parametrized test
params = [(LinearSearch(array, 5).search(), array.index(5)),
          (BinarySearch(array, 83).search(), array.index(83)),
          (NaiveStringSearch(string, 'rm').search(), string.find('rm')),
          (KMPStringSearch(string, 'Ed').search(), string.find('Ed'))]


@pytest.mark.parametrize("test_input, excepted", params)
def test_compare(test_input, excepted):
    assert test_input == excepted



