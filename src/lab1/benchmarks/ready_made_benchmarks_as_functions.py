from src.utils.benchmark_functions import calls_counter, elapsed_time, occupied_memory
from random import randint
from src.lab1.lib.sort_algo import (bubble_sort, insertion_sort,
                                    shell_sort, quick_sort)


def callable_benchmark_calls(start, end, step):
    """Callable benchmark for finding the number of operations"""
    average_calls = ([], [], [], [])
    size_of_array = []

    for size in range(start, end, step):
        array = [randint(-64, 63) for i in range(size)]

        # multiply by 2 because swap equals 2 replace
        average_calls[0].append(calls_counter(bubble_sort, array, lines_to_print='sorter.py') * 2)
        average_calls[1].append(calls_counter(insertion_sort, array, lines_to_print='sorter.py'))
        average_calls[2].append(calls_counter(shell_sort, array, lines_to_print='sorter.py'))
        average_calls[3].append(calls_counter(quick_sort, array,  lines_to_print='sorter.py') * 2)

        size_of_array.append(size)

        print(f"Size of array is {size}")
        print(f"Average calls bubble sort = {average_calls[0][-1]}, ins sort = {average_calls[1][-1]},",
              f"Shell sort = {average_calls[2][-1]}, quick sort = {average_calls[3][-1]}")

    return size_of_array, average_calls


def callable_benchmark_memory(start, end, step):
    """Callable benchmark for finding spent memory"""
    memory = ([], [], [], [])
    size_of_array = []

    for size in range(start, end, step):
        array = [randint(-64, 63) for i in range(size)]

        memory[0].append(occupied_memory(bubble_sort, array))
        memory[1].append(occupied_memory(insertion_sort, array))
        memory[2].append(occupied_memory(shell_sort, array))
        memory[3].append(occupied_memory(quick_sort, array))

        size_of_array.append(size)

        print(f"Size of array is {size}")
        print(f"Memory used by bubble sort = {memory[0][-1]}, ins sort = {memory[1][-1]},",
              f"Shell sort = {memory[2][-1]}, quick sort = {memory[3][-1]}")

    return size_of_array, memory


def callable_benchmark_time(start, end, step):
    """Callable benchmark for finding used time"""
    time = ([], [], [], [])
    size_of_array = []

    for size in range(start, end, step):
        array = [randint(-64, 63) for i in range(size)]

        time[0].append(elapsed_time(bubble_sort, array))
        time[1].append(elapsed_time(insertion_sort, array))
        time[2].append(elapsed_time(shell_sort, array))
        time[3].append(elapsed_time(quick_sort, array))

        size_of_array.append(size)

        print(f"Size of array is {size}")
        print(f"Time used by bubble sort = {time[0][-1]}, ins sort = {time[1][-1]},",
              f"Shell sort = {time[2][-1]}, quick sort = {time[3][-1]}")

    return size_of_array, time
