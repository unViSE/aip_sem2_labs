import matplotlib.pyplot as plt

from src.lab1.benchmarks.ready_made_benchmarks_as_functions import callable_benchmark_memory
from numpy import polyfit, poly1d


from src.lab1.benchmarks.const_for_benchmarks import (TEST_BEGIN, TEST_END, TEST_STEP,
                                                      REAL_BEGIN, REAL_END, REAL_STEP)


test_size_of_array, test_memory = callable_benchmark_memory(TEST_BEGIN, TEST_END, TEST_STEP)
real_size_of_array = list(range(REAL_BEGIN, REAL_END, REAL_STEP))

p0_coff = polyfit(test_size_of_array, test_memory[0], 1)
p1_coff = polyfit(test_size_of_array, test_memory[1], 1)
p2_coff = polyfit(test_size_of_array, test_memory[1], 1)
p3_coff = polyfit(test_size_of_array, test_memory[2], 1)

p0 = poly1d(p0_coff)
p1 = poly1d(p1_coff)
p2 = poly1d(p2_coff)
p3 = poly1d(p3_coff)

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Matplotlib for visualization space complexity of algorithms
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
figure, ax = plt.subplots()

ax.set(title="Space complexity of algorithms", xlabel="Array size", ylabel="Mb")

plt.plot(test_size_of_array, p0(real_size_of_array), label="Bubble")
plt.plot(test_size_of_array, p1(real_size_of_array), label="Insertion sort")
plt.plot(test_size_of_array, p2(real_size_of_array), label="Shell sort")
plt.plot(test_size_of_array, p3(real_size_of_array), label="Quick sort")

ax.grid(), ax.legend()
plt.show()