import os


# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Remove a unused files from dir
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
def remove_files_from_dir(type_of_file=None):
    """Function for deleting all unnecessary files

    @param type_of_file: str, optional
        Type of deleting files
    @return: Void
    """
    path = os.getcwd()
    files = [f for f in os.listdir(path) if f.endswith(f".{type_of_file}")]
    for f in files:
        os.remove(os.path.join(path, f))