Do not use this benchmark with the standard Python interpreter!!! 
Install either pypy or cpython. 

Recommended interpreter - pypy3.7.

Test values (in "const_for_benchmark.py") are the original small values needed to build the original math function.
Further, using the least squares method (function in numpy library), a polynomial similar to our original math function 
will be built, only for large values that require a lot of computing power. In this way, we can show the graph for 
large values without using too much of the processing power of our computer.