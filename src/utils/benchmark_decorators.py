import cProfile
import pstats

from functools import wraps
from timeit import default_timer
from memory_profiler import memory_usage


def multiple_call_decorator(*decorators):
    """Decorator for the convenience of calling multiple decorators"""
    def wrapper(function):
        for decorator in reversed(decorators):
            function = decorator(function)
        return function
    return wrapper


def elapsed_time(function):
    """Decorator for getting the elapsed time"""
    @wraps(function)
    def wrapper(*args, **kwargs):
        start_clock = default_timer()
        result = function(*args, **kwargs)
        print(f"The elapsed time is {default_timer() - start_clock} s")
        return result
    return wrapper


def occupied_memory(function):
    """Decorator for getting the occupied RAM for any function"""
    @wraps(function)
    def wrapper(*args, **kwargs):
        mem_usage_old = memory_usage((function, (*args,), {**kwargs}))
        result = function(*args, **kwargs)
        mem_usage_new = memory_usage((function, (*args, ), {**kwargs}))
        print(f"Memory occupied by function is "
              f"{sum(mem_usage_new) / len(mem_usage_new) - sum(mem_usage_old) / len(mem_usage_old)} mb")

        return result
    return wrapper


def profile(output_filename=None, sort_by='cumulative', lines_to_print=None, strip_dirs=False):
    """A profiler decorator

    @param output_filename: str, optional. Default is None(<function name>.txt)
        Path of the output file. If only name of the file is given, it's
        saved in the current directory.
        If it's None, the name of the decorated function is used.
    @param sort_by: str or SortKey enum or tuple/list of str/SortKey enum, optional
        Sorting criteria for the Stats object.
        For a list of valid string and SortKey refer to:
        https://docs.python.org/3/library/profile.html#pstats.Stats.sort_stats
    @param lines_to_print: int, str, optional. Default is None
        Number of lines to print. Default (None) is for all the lines.
        This is useful in reducing the size of the printout, especially
        that sorting by 'cumulative', the time consuming operations
        are printed toward the top of the file.
    @param strip_dirs: bool, optinal. Default is False
        Whether to remove the leading path info from file names.
        This is also useful in reducing the size of the printout
    @return: Profile of the decorated function"""

    def inner(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            _output_file = output_filename or f"{function.__name__}.txt"
            pr = cProfile.Profile()
            pr.enable()
            result = function(*args, **kwargs)
            pr.disable()
            pr.dump_stats(_output_file)

            with open(_output_file, 'w', encoding='utf-8') as f:
                ps = pstats.Stats(pr, stream=f)
                if strip_dirs:
                    ps.strip_dirs()
                if isinstance(sort_by, (tuple, list)):
                    ps.sort_stats(*sort_by)
                else:
                    ps.sort_stats(sort_by)
                ps.print_stats(lines_to_print)

            return result
        return wrapper
    return inner
