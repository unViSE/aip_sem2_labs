def get_prefix_as_array(target, target_length):
    """Function for getting the prefix as an array

    @param target: var
        The variable to be found
    @param target_length: int
        The length of the target
    @return:
    """
    prefix_array = [0] * target_length
    n = 0
    m = 1
    while m != target_length:
        if target[m] == target[n]:
            n += 1
            prefix_array[m] = n
            m += 1
        elif n != 0:
            n = prefix_array[n-1]
        else:
            prefix_array[m] = 0
            m += 1
    return prefix_array


def compare(first, second, choice=0):
    """Function for comparing the first to the second

    @param first: var
        First variable
    @param second:
        Second variable
    @param choice: int[0:3]
        Parameter to select the desired condition
    @return: bool
    """
    if choice == 0:
        return first == second
    elif choice == 1:
        return first > second
    elif choice == 2:
        return first < second
    elif choice == 3:
        return first != second
