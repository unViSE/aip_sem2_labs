import random

from src.libs.lib_lab1 import callable_functions as cf


def main():
    array_size = input("Input the size of array >> ")

    if not array_size.isdigit():
        raise TypeError("Incorrect choice!")

    array = [random.randint(-64, 63) for i in range(int(array_size))]

    print('Select the desired option:')

    algorithm = input("\t1 - Sort the array using the Bubble sort\n"
                      "\t2 - Sort the array using the Insertion sort\n"
                      "\t3 - Sort the array using the Shell sort\n"
                      "\t4 - Sort the array using the Quicksort\n"
                      "\nChoice >> ")

    reversed = input("\n\t0 - not reversed sorted array\n"
                     "\t1 - reverse the sorted array\n"
                     "\nChoice >> ")

    if algorithm not in ["1", "2", "3", "4"] or reversed not in ['0', '1']:
        raise TypeError("Incorrect choice!")

    print("\n", f"Unsorted array: {array}", "\n")

    if algorithm == "1":
        print("---------------BUBBLE_SORT---------------")
        print(f"Sorted array {cf.bubble_sort_with_decorators(array, reverse=eval(reversed))}")

    elif algorithm == "2":
        print("---------------INSERTION_SORT---------------")
        print(f"Sorted array {cf.insertion_sort_with_decorators(array, reverse=eval(reversed))}")

    elif algorithm == "3":
        print("---------------SHELL_SORT---------------")
        print(f"Sorted array {cf.shell_sort_with_decorators(array, reverse=eval(reversed))}")

    elif algorithm == "4":
        print("---------------QUICK_SORT---------------")
        print(f"Sorted array {cf.quick_sort_with_decorators(array, reverse=eval(reversed))}")


if __name__ == "__main__":
    main()
