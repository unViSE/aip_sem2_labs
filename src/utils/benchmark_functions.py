import memory_profiler
import timeit
import pstats
import cProfile


def elapsed_time(function, array, repeats=1):
    """Function for getting the elapsed time"""
    result = 0
    for i in range(repeats):
        start_clock = timeit.default_timer()
        function(array)
        end_clock = timeit.default_timer() - start_clock

        result += end_clock
    return result/repeats


def occupied_memory(function, *args, **kwargs):
    """Function for getting the occupied RAM for any function"""
    memory_usage = memory_profiler.memory_usage((function, (*args,), {**kwargs, }), interval=1)
    return sum(memory_usage) / len(memory_usage)


# TODO: rework function for cls
def calls_counter(callable_object, *params_for_object, file_name=None, lines_to_print=None):
    """A function that first profiles the desired function,
    and then analyzes and corrects the necessary data from the profiler file

    @param callable_object: cls
        The object to be called
    @param params_for_object: list
        Required parameters for calling an object
    @param file_name: str, optional. Default is None.
        Output file name
    @param lines_to_print: str, optional. Default is None
        Param to cProfiler function for convenient call sorting
    @return: The number of operations of the called function
    """
    if file_name is None:
        file_name = f'{callable_object.__name__}.txt'
    else:
        file_name = f'{file_name}.txt'

    with open(file_name, 'w') as f:
        pr = cProfile.Profile()
        pr.enable()
        callable_object(*params_for_object)
        pr.disable()
        p = pstats.Stats(pr, stream=f)
        p.strip_dirs().sort_stats().print_stats(lines_to_print)

    with open(file_name) as f:
        lines = f.readlines()
        lines = [line.strip() for line in lines]
        lines = [line for line in lines if line not in '']
        return int(lines[4].split()[0])
